package com.example.tokoku.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;

import com.example.tokoku.dao.AbstractDao;
import com.example.tokoku.dao.ProductDao;
import com.example.tokoku.model.Product;

@Repository("productDao")
public class ProductDaoImpl extends AbstractDao<Integer, Product> implements ProductDao {
 
    public Product findById(int id) {
        return getByKey(id);
    }
 
    public void save(Product product) {
        persist(product);
    }
 
    public void delete(int id) {
    	Product p = getByKey(id);
    	delete(p);
    }
 
    public List<Product> findAll() {
        Criteria criteria = createEntityCriteria();
        return (List<Product>) criteria.list();
    } 
}
