package com.example.tokoku.dao;

import java.util.List;

import com.example.tokoku.model.Product;

public interface ProductDao {
	 
    Product findById(int id);
 
    void save(Product product);
     
    void delete(int id);
     
    List<Product> findAll();
 
}
