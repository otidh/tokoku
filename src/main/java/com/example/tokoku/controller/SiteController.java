package com.example.tokoku.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class SiteController {
	
	@RequestMapping(method = RequestMethod.GET)
    public ModelAndView home(){
    	ModelAndView mv = new ModelAndView("tokoku.home");
        return mv;
    }
}
