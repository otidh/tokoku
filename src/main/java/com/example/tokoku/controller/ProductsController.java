package com.example.tokoku.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.example.tokoku.model.Product;
import com.example.tokoku.service.ProductService;

@Controller
@RequestMapping("/products")
public class ProductsController {

	@Autowired
	private ProductService productService = null;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView showProducts() {
		ModelAndView mv = new ModelAndView("tokoku.products");

		List<Product> products = productService.findAll();
		mv.addObject("products", products);

		return mv;
	}
}