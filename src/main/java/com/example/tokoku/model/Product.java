package com.example.tokoku.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
 
@Entity
@Table(name="product")
public class Product {
 
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
 
    @Column(length=50, nullable=false)
    private String name;
    
    @Column(nullable=false)
    private String description;
    
    @Column(nullable=false)
    private Double price;
    
    @Column(nullable = false,updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationTime = null;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Date getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}
 
	public void doCopyUpdateFieldsFrom(Product fromProduct) {
		setName(fromProduct.getName());
		setDescription(fromProduct.getDescription());
		setPrice(fromProduct.getPrice());
    }
     
}
