package com.example.tokoku.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.tokoku.dao.ProductDao;
import com.example.tokoku.model.Product;
import com.example.tokoku.service.ProductService;

@Service("productService")
@Transactional
public class ProductServiceImpl implements ProductService {
 
    @Autowired
    private ProductDao dao;
     
    public Product findById(int id) {
        return dao.findById(id);
    }
 
    public void save(Product product) {
        dao.save(product);
    }
    
    public void updateProduct(Product product) {
        Product entity = dao.findById(product.getId());
        if(entity!=null){
        	entity.doCopyUpdateFieldsFrom(product);
        }
    }
 
    public void delete(int id) {
        dao.delete(id);
    }
     
    public List<Product> findAll() {
        return dao.findAll();
    }
     
}
