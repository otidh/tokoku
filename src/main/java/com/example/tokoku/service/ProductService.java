package com.example.tokoku.service;

import java.util.List;

import com.example.tokoku.model.Product;

public interface ProductService {
	 
    Product findById(int id);
     
    void save(Product product);
     
    void delete(int id);
 
    List<Product> findAll(); 
     
}
