<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<div class="col-md-3">
	<p class="lead">Product Categories</p>
	<div class="list-group">
		<a href="#" class="list-group-item">Category 1</a> <a href="#"
			class="list-group-item">Category 2</a> <a href="#"
			class="list-group-item">Category 3</a>
	</div>
</div>
<div class="col-md-9">
	<div class="row">
		<c:forEach items="${products}" var="product">
			<div class="col-sm-4 col-lg-4 col-md-4">
				<div class="thumbnail">
					<img src="http://placehold.it/320x150" alt="">
					<div class="caption">
						<h4 class="name">${product.name}</h4>
						<h4><fmt:formatNumber value="${product.price}" type="currency" currencySymbol="Rp"/></h4>						
						<p class="description">${product.description}</p>
					</div>
				</div>
			</div>
		</c:forEach>
	</div>
</div>